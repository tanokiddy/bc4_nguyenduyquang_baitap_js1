// BT1
function tinhTienLuong() {
  var soNgay = document.getElementById("txt-so-ngay").value * 1;
  var luongNgay = document.getElementById("txt-luong-1-ngay").value * 1;
  // console.log({ soNgay, luongNgay });

  var tienLuong = soNgay * luongNgay;
  // console.log("tienLuong: ", tienLuong);
  document.getElementById("result1").innerHTML = `<h1> Total: ${tienLuong} VND
   </h1>`;
}

// BT2
function tinhTrungBinh() {
  var s1 = document.getElementById("so1").value * 1;
  var s2 = document.getElementById("so2").value * 1;
  var s3 = document.getElementById("so3").value * 1;
  var s4 = document.getElementById("so4").value * 1;
  var s5 = document.getElementById("so5").value * 1;
  // console.log({ s1, s2, s3, s4, s5 });
  var trungBinhSo = (s1 + s2 + s3 + s4 + s5) / 5;
  // console.log("trungBinhSo: ", trungBinhSo);
  document.getElementById("result2").innerHTML = `<h1> Total: ${trungBinhSo} 
     </h1>`;
}

// BT3
function tinhQuyDoi() {
  var soTien$ = document.getElementById("txt-sotiencandoi").value * 1;
  var giaTri$ = document.getElementById("txt-giatri$").value * 1;
  // console.log({ soTien$, giaTri$ });

  var tienVND = soTien$ * giaTri$;
  // console.log("tienVND: ", tienVND);
  document.getElementById("result3").innerHTML = `<h1> Total: ${tienVND} VND
   </h1>`;
}

// BT4
function tinhChuVi() {
  var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
  var chieuRong = document.getElementById("txt-chieu-rong").value * 1;
  // console.log({ chieuDai, chieuRong });

  var chuVi = 0;
  if (chieuRong >= chieuDai) {
    chuVi = NaN;
  } else {
    chuVi = (chieuDai + chieuRong) * 2;
    // console.log("chuVi: ", chuVi);
  }
  document.getElementById("resultchuvi").innerHTML = `<h1> P = ${chuVi} 
     </h1>`;
}
function tinhDienTich() {
  var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
  var chieuRong = document.getElementById("txt-chieu-rong").value * 1;

  var dienTich = 0;
  if (chieuRong >= chieuDai) {
    dienTich = NaN;
  } else {
    dienTich = chieuDai * chieuRong;
    // console.log("dienTich: ", dienTich);
  }
  document.getElementById("resultdientich").innerHTML = `<h1> S = ${dienTich} 
     </h1>`;
}

// BT5
function tinhTongKySo() {
  var so2ChuSo = document.getElementById("txt-so-2-chu-so").value * 1;
  var tongKySo = 0;
  if (so2ChuSo < 10) {
    so2ChuSo = NaN;
    tongKySo = NaN;
  } else {
    so2ChuSo = document.getElementById("txt-so-2-chu-so").value * 1;
    tongKySo = (so2ChuSo % 10) + Math.floor(so2ChuSo / 10);
    // console.log({ so2ChuSo, tongKySo });
  }
  document.getElementById(
    "result5"
  ).innerHTML = `<h1> Tổng ký số: ${tongKySo}</h1>`;
}
